package com.example.dockerdemo.actuator;

import org.springframework.boot.actuate.info.Info;
import org.springframework.stereotype.Component;

@Component
public class InfoContributor implements org.springframework.boot.actuate.info.InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("App description ","A Springboot service to demo DockerFile usage");
    }
}
