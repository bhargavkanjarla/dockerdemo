package com.example.dockerdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class demoController {

    @GetMapping("")
    public String welcome(){
        return "Hello! welcome to Docker Session:)";
    }
}
