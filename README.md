# Getting Started

### Demo Application to Run DockerFile


### START and RUN

default port : 8080

start - ./gradlew clean build

run - /gradlew bootRun

##endpoints

localhost:8080

localhost:8080/actuator/info

localhost:8080/actuator/health
